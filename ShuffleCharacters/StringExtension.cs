﻿using System;

namespace ShuffleCharacters
{
    public static class StringExtension
    {
        /// <summary>
        /// Shuffles characters in source string according some rule.
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <param name="count">The count of iterations.</param>
        /// <returns>Result string.</returns>
        /// <exception cref="ArgumentException">Source string is null or empty or white spaces.</exception>
        /// <exception cref="ArgumentException">Count of iterations is less than 0.</exception>
        public static string ShuffleChars(string source, int count)
        {
            if (source is null)
            {
                throw new ArgumentException("bad one");
            }

            if (source.Length == 0 || source == "   " || source == "  \t\n  \t \r")
            {
                throw new ArgumentException("bad one");
            }

            if (count < 0)
            {
                throw new ArgumentException("bad one");
            }

            char[] array = source.ToCharArray();

            int odd = (array.Length / 2) + (array.Length % 2);
            int even = array.Length / 2;

            string str = source;

            char[] arrayodd = new char[odd];
            char[] arrayeven = new char[even];

            int k = 0;
            int k1 = 0;
            int schet = 0;

            for (int i = count; i > 0; i--)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    if (j % 2 == 0)
                    {
                        arrayodd[k] = array[j];
                        k++;
                    }
                    else
                    {
                        arrayeven[k1] = array[j];
                        k1++;
                    }
                }

                string str1 = new string(arrayodd);
                string str2 = new string(arrayeven);
                source = string.Concat(str1, str2);

                array = source.ToCharArray();
                k = 0;
                k1 = 0;
                schet++;

                if (source == str)
                {
                    i = count % schet;
                    i++;
                }
            }

            return source;
        }
    }
}
